<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'active' => true,
            'status' => true,
            'mobile' => '01552521736',
            'admin' => true,
            'password' => Hash::make('secret'),
        ];
        App\User::create($data);
    }
}
