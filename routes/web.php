<?php

/*

| Web Routes
|--------------------------------------------------------------------------
 => '|--------------------------------------------------------------------------'|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//, 'active'
Route::group(['namespace' => 'Site', 'as' => 'site.', 'middleware' => ['auth']], function(){
    Route::group(['as' => 'users.', 'prefix' => 'users'], function(){
        Route::get('show', 'UsersController@show')->name('show');
    });

    Route::group(['as' => 'auth.'], function(){
        Route::get('edit', 'UsersController@edit')->name('edit');
        Route::post('update', 'UsersController@update')->name('update');
        Route::post('delete', 'UsersController@delete')->name('delete');
    });
});

Route::group(['namespace' => 'Admin', 'prefix' => 'dashboard', 'middleware' => ['auth', 'admin']], function(){
    Route::get('/', 'DashboardController@index')->name('home');
    Route::group(['prefix' => 'users', 'as' => 'users.'], function(){
        Route::get('/', 'UsersController@index')->name('index');
        Route::get('{id}/show', 'UsersController@show')->name('show');
        Route::get('{id}/edit', 'UsersController@edit')->name('edit');
        Route::post('{id}/update', 'UsersController@update')->name('update');
        Route::post('{id}/delete', 'UsersController@delete')->name('delete');
    });
});

