## Run App :

please use the following commands :
- `composer install`
- `composer dump-autoload`
- `php artisan migrate:refresh --seed`
- `php artisan storage:link`
username: admin
email: admin@admin.com
password: secret
