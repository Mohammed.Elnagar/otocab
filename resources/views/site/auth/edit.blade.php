@extends('layouts.app')
@section('content')
    <div class="col-md-9">
                @php
                    $user = auth()->user();
                @endphp

                <div class="card">
                <div class="card-header">Edit user </div>

                <div class="card-body">
                    <a target="_plank" href="/storage/{{ $user->file }}">Image / pdf </a>
                    <form method="POST" action="{{ route('site.auth.update') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">{{ __('file') }}</label>

                            <div class="col-md-6">
                                <input id="file" type="file" class="form-control @error('file') is-invalid @enderror" name="file">
                                @error('file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @if($user->mobile)
                            <p class="text-muted">
                                <div class="wrapper-mobiles ">
                                    @foreach (json_decode($user->mobile) as $key => $mob)
                                        <div id="mobile-container{{ $key }}" class="form-group row">
                                            <label for="mobile{{ $key }}" class="col-md-4 col-form-label text-md-right">Mobile</label>
                                            <div class="col-md-5">
                                                <input id="mobile{{ $key }}" value="{{ $mob }}" type="text" class="form-control " name="mobile[]">
                                            </div>
                                            @if($key != 0)
                                                <div class="col-md-1">
                                                    <button class="btn btn-danger remove-mobile" mobile="mobile-container{{$key}}"> X </button>
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </p>
                        @endif

                        <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <span class="btn btn-primary add-mobile"> Add mobile</span>
                                </div>
                            </div>

                        <div class="form-group row mt-4">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update user
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>

               </div>
           </div>
    </div>


@endsection
@section('script')
    <script>
        jQuery(document).ready(function(){

            let index = 2;
            $(document).on('click', '.add-mobile', function(e){
                e.preventDefault()
                const $template =
                `<div id="mobile-container${index}" class="form-group row">
                    <label for="mobile${index}" class="col-md-4 col-form-label text-md-right"> Mobile</label>
                    <div class="col-md-5">
                        <input id="mobile${index}" type="text" class="form-control" name="mobile[]">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-danger remove-mobile" mobile="mobile-container${index}"> X </button>
                    </div>
                </div>`
                $('.wrapper-mobiles').append($template)
                index = index + 1
            })

            $(document).on('click', '.remove-mobile', function(e){
                e.preventDefault()
                const $id = $(this).attr('mobile')
                $(`#${$id}`).remove();
            })

    })

    </script>
@endsection
