@extends('layouts.app')
@section('content')
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">Users Show</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @if (auth()->user()->status)
                    <h3>This acount is blocked </h3>

                    {!! auth()->user()->reason_block?'<p><strong>Reason block: </strong>'.auth()->user()->reason_block.'</p>':'' !!}

                @else
                    @if(auth()->user()->active)
                        <h3>Approved</h3>
                        @forelse ($users as $user)
                            {{ $user->name }} <br>
                        @empty
                            Dont have users
                        @endforelse
                    @else
                        your photo or pdf file not approved
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection
