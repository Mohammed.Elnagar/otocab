<footer class="main-footer">
  <strong>Copyright &copy; {{ now()->format('Y') }} <a href="https://m.elnagar.mycv.studio">Mohammed elnagar</a>.</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 3.0.3-pre
  </div>
</footer>
