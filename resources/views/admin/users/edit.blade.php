@extends('admin.layouts.app')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <section class="content">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Edit User {{ $user->name }}</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
                <a target="_plank" href="/storage/{{ $user->file }}">Image / pdf </a>
            <p> <strong> Username: </strong> {{ $user->name }}</p>
            <p> <strong> Email: </strong> {{ $user->email }}</p>
            @if($user->mobile)
              <p class="text-muted">
                @foreach (json_decode($user->mobile) as $key => $mob)
                    <strong> Mobile {{ $key+=1 }}: </strong> {{ $mob }}<br>
                @endforeach
              </p>
            @endif
              <form action="{{ route('users.update', $user->id) }}" role="form" method="POST">
                  @csrf
                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                      <input type="checkbox" {{ $user->active ? 'checked="checked"':'' }} name="active" class="custom-control-input" id="active">
                      <label class="custom-control-label" for="active">Switch Approved/not approverd </label>
                    </div>
                  </div>

                <div class="form-group">
                  <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="checkbox" {{ $user->status? 'checked="checked"':'' }} name="status" class="custom-control-input" id="status">
                    <label class="custom-control-label" for="status">Switch block/un block </label>
                  </div>
                </div>

                <div class="form-group">
                    <div class="custom-control">
                        <label class="custom-control" for="reason_block"> Reason block </label>
                        <textarea name="reason_block" id="reason_block" cols="30" rows="10"> {{$user->reason_block}}</textarea>
                    </div>
                </div>

                <button class="btn btn-primary" type="submit"> Update </button>
            </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
