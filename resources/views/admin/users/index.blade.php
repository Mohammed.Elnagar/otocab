@extends('admin.layouts.app')

@section('content')

  <!-- Main content -->
	<section class="content">
		<div class="container-fluid">
      <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Username</th>
                  <th>email</th>
                  <th>Status</th>
                  <th>Active</th>
                  <th> Image / pdf</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)

                    <tr>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->status == 0 ? "Un block" : "Block" }}</td>
                      <td>{{ $user->active == 1 ? "Approved" : "Not approved"}}</td>
                    <td><a target="_plank" href="/storage/{{ $user->file }}">Image / pdf </a></td>
                      <th>
                        <a href="{{ route('users.edit',$user->id) }}" class="btn btn-primary"> Edit </a>
                        <a href="{{ route('users.show',$user->id) }}" class="btn btn-success"> Show </a>
                        <a href="#" class="btn btn-danger delete-btn" renderURL="{{ route('users.delete',$user->id) }}"> Delete </a>
                      </th>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Username</th>
                  <th>email</th>
                  <th>Status</th>
                  <th>Active</th>
                  <th> Image / pdf</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
            <span onclick="getLocation()" class="col-6 btn btn-primary mt-3 mb-3"> Show map</span>
            <div class="col-12">

                <p id="mapholder"></p>
                <p id="demo"></p>
            </div>
        </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
		</div><!-- /.container-fluid -->
	</section>
  <!-- /.content -->
  <form  id="form-delete" action="" method="POST">
    @csrf
  </form>
@endsection

@section('script')

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyBlMQ9nSivKFGdPmaWXINolMOgx0Au79I4"></script>

    <script>
            var x=document.getElementById("demo");
            function getLocation()
              {
              if (navigator.geolocation)
                {
                navigator.geolocation.getCurrentPosition(showPosition,showError);
                }
              else{x.innerHTML="Geolocation is not supported by this browser.";}
              }

            function showPosition(position)
              {
              var lat=position.coords.latitude;
              var lon=position.coords.longitude;
              var latlon=new google.maps.LatLng(lat, lon)
              var mapholder=document.getElementById('mapholder')
              mapholder.style.height='250px';
              mapholder.style.width='100%';

              var myOptions={
              center:latlon,zoom:14,
              mapTypeId:google.maps.MapTypeId.ROADMAP,
              mapTypeControl:false,
              navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
              };
              var map=new google.maps.Map(document.getElementById("mapholder"),myOptions);
              var marker=new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
              }

            function showError(error)
              {
              switch(error.code)
                {
                case error.PERMISSION_DENIED:
                  x.innerHTML="User denied the request for Geolocation."
                  break;
                case error.POSITION_UNAVAILABLE:
                  x.innerHTML="Location information is unavailable."
                  break;
                case error.TIMEOUT:
                  x.innerHTML="The request to get user location timed out."
                  break;
                case error.UNKNOWN_ERROR:
                  x.innerHTML="An unknown error occurred."
                  break;
                }
              }
            </script>

@endsection
