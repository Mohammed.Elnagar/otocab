<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class updateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->password);
        if(request()->password){
            $rules = [
                //
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,'.auth()->user()->id,
                'password' => 'required|string|min:8|confirmed',
                'file' => 'nullable|file|mimes:gif,png,jpeg,jpg,pdf|max:2000'
            ];
        }else{
            $rules = [
                //
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,id,'.auth()->user()->id,
                'file' => 'nullable|file|mimes:gif,png,jpeg,jpg,pdf|max:2000'
            ];
        }
        return $rules;
    }
}
