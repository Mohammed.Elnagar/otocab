<?php

namespace App\Http\Controllers\Site;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Site\updateUserRequest;

class UsersController extends Controller
{
    //

    public function show(){
        $users = User::whereAdmin(false)->where('id', '<>', auth()->user()->id)->get();
        return view('site.users.show', compact('users'));
    }

    public function edit(){
        return view('site.auth.edit');
    }

    public function update(updateUserRequest $requset){
        $user = auth()->user();
        if (request()->hasFile('file')) {
            Storage::delete('public/'.$user->file);
            $file_path = base_path() . '/storage/app/public/';
            $file = $requset['file'];
            $extension = $file->extension();
            $newFileName = time() . '_' . md5((string)mt_rand(1, 1000000)) . '.' . $extension;
            $file->storeAs('public', $newFileName);
        }
        $requset['file']     ? $fileName = $newFileName : $fileName = $user->file;
        $requset['password'] ? $password = Hash::make($requset['password']) : $password = $user->password;

        $user->update([
                'name' => $requset['name'],
                'email' => $requset['email'],
                'file' => $fileName,
                'mobile' => json_encode($requset['mobile']),
                'password' => $password,
            ]);
        alert()->success('Success', 'Your Data Updated')->autoclose(6000);
        return redirect()->back();
    }

    public function delete($id, Request $requset){
        $user = User::findOrfail($id);
        $user->delete();
        alert()->success('Success', 'Your Data Deleted')->autoclose(6000);
        return redirect()->route('users.index');
    }

}
