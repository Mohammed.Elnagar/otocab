<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    //
    public function index(){
        $users = User::where('id', '<>', auth()->user()->id)
        ->where('admin', false)
        ->get();
        return view('admin.users.index', compact('users'));
    }

    public function show($id){
        $user = User::findOrfail($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($id){
        $user = User::findOrfail($id);
        return view('admin.users.edit', compact('user'));
    }

    public function update($id, Request $requset){
        $user = User::findOrfail($id);
        $requset->has('active')? $active = true : $active = false;
        $requset->has('status')? $status = true : $status = false;
        $user->status = $status;
        $user->active = $active;
        $user->reason_block = $requset->reason_block;
        $user->update();
        alert()->success('Success', 'Your Data Updated')->autoclose(6000);
        return redirect()->back();
    }

    public function delete($id, Request $requset){
        $user = User::findOrfail($id);
        $user->delete();
        alert()->success('Success', 'Your Data Deleted')->autoclose(6000);
        return redirect()->route('users.index');
    }


}
